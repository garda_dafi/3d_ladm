<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CesiumRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'viewer_name' => 'required|alpha_dash|unique:cesium,viewer_name,'.$this->id,
            'viewer_option' => 'nullable',
            'script' => 'nullable',
            'description' => 'nullable',
        ];
    }
}
