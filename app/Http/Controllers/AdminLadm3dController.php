<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Ladm3dRequest;
use App\Ladm3d;
use App\Page;
use File, Session;

class AdminLadm3dController extends Controller
{
  public function index()
  {
      $docs = Ladm3d::get();
      $page = Page::where('name', Ladm3d::name())->first();

      return view('admin.page.ladm3d.index', [
          'docs' => $docs,
          'page' => $page
      ]);
  }

  public function create()
  {
      return view('admin.page.ladm3d.form', [
          'ladm3d' => new Ladm3d
      ]);
  }

  public function store(Ladm3dRequest $request)
  {
      $ladm3d = new Ladm3d;

      $ladm3d->title = $request->title;
      $ladm3d->content_text = $request->content_text;
      $ladm3d->button_url = $request->button_url;
      $ladm3d->button_title = $request->button_title;

      if($request->image_url){
          $image_url                   = time().'.'.$request->image_url->getClientOriginalExtension();
          $request->image_url->move(public_path('images/ladm3d'), $image_url);
          $ladm3d->image_url               = $image_url;
      }

      if($ladm3d->save()) {
          Session::flash('success-saved');
          return redirect()->route('admin.3d_ladm.index');
      }
  }

  public function edit($id)
  {
      $ladm3d = Ladm3d::find($id);

      return view('admin.page.ladm3d.form', [
          'ladm3d' => $ladm3d
      ]);
  }

  public function update(Ladm3dRequest $request, $id)
  {
      $ladm3d = Ladm3d::find($id);

      $ladm3d->title = $request->title;
      $ladm3d->content_text = $request->content_text;
      $ladm3d->button_url = $request->button_url;
      $ladm3d->button_title = $request->button_title;

      if($request->image_url){
          Session::flash('success-saved');
          File::delete(public_path('images/ladm3d/'.$ladm3d->image_url));
          $image_url                   = time().'.'.$request->image_url->getClientOriginalExtension();
          $request->image_url->move(public_path('images/ladm3d'), $image_url);
          $ladm3d->image_url               = $image_url;
      }

      if($ladm3d->save()) {
          return redirect()->route('admin.3d_ladm.index');
      }
  }

  public function destroy($id)
  {
      $ladm3d = Ladm3d::find($id);

      if($ladm3d->delete()){
          File::delete(public_path('images/ladm3d/'.$ladm3d->image_url));
          Session::flash('success-delete');
          return redirect()->back();
      }

      return redirect()->back();
  }
}
