<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use File, Session;

class AdminPageController extends Controller
{
    public function edit($page)
    {
        $page = Page::where('name', $page)->first();

        return view('admin.page.page', [
            'page' => $page
        ]);
    }
    public function store(Request $request, $name)
    {
        $page = Page::where('name', $name)->first();
        if(!$page) {
            $page = new Page;
        }
        $page->name = $request->name;
        $page->small_title = $request->small_title;
        $page->big_title = $request->big_title;


        if($request->thumbnail){
            File::delete(public_path('images/page/thumbnail/'.$name.'/'.$page->thumbnail));
            $thumbnail                   = time().'.'.$request->thumbnail->getClientOriginalExtension();
            $request->thumbnail->move(public_path('images/page/thumbnail/'.$name), $thumbnail);
            $page->thumbnail               = $thumbnail;
        }

        if($request->background){
            File::delete(public_path('images/page/background/'.$name.'/'.$page->background));
            $background                   = time().'.'.$request->background->getClientOriginalExtension();
            $request->background->move(public_path('images/page/background/'.$name), $background);
            $page->background               = $background;
        }
        if($page->save()) {
            Session::flash('success-saved');
            return redirect()->back();
        }

        Session::flash('error-saved');
        return redirect()->back();
    }
}
