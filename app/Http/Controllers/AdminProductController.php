<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Product;
use App\Page;
use File, Session;

class AdminProductController extends Controller
{
    public function index()
    {
        $docs = Product::get();
        $page = Page::where('name', Product::name())->first();
        return view('admin.page.product.index', [
            'docs' => $docs,
            'page' => $page
        ]);
    }

    public function create()
    {
        return view('admin.page.form', [
            'data' => new Product,
            'page' => Product::name()
        ]);
    }

    public function store(ProductRequest $request)
    {
        $product = new Product;

        $product->title = $request->title;
        $product->content_text = $request->content_text;
        $product->button_url = $request->button_url;
        $product->button_title = $request->button_title;
        $product->image_rotate = $request->image_rotate;

        if($request->image_url){
            $image_url                   = time().'.'.$request->image_url->getClientOriginalExtension();
            $request->image_url->move(public_path('images/product'), $image_url);
            $product->image_url               = $image_url;
        }

        if($product->save()) {
            Session::flash('success-saved');
            return redirect()->route('admin.product.index');
        }
    }

    public function edit($id)
    {
        $product = Product::find($id);

        return view('admin.page.form', [
            'data' => $product,
            'page' => Product::name()
        ]);
    }

    public function update(ProductRequest $request, $id)
    {
        $product = Product::find($id);

        $product->title = $request->title;
        $product->content_text = $request->content_text;
        $product->button_url = $request->button_url;
        $product->button_title = $request->button_title;
        $product->image_rotate = $request->image_rotate;

        if($request->image_url){
            Session::flash('success-saved');
            File::delete(public_path('images/product/'.$product->image_url));
            $image_url                   = time().'.'.$request->image_url->getClientOriginalExtension();
            $request->image_url->move(public_path('images/product'), $image_url);
            $product->image_url               = $image_url;
        }

        if($product->save()) {
            return redirect()->route('admin.product.index');
        }
    }

    public function destroy($id)
    {
        $product = Product::find($id);

        if($product->delete()){
            File::delete(public_path('images/product/'.$product->image_url));
            Session::flash('success-delete');
            return redirect()->back();
        }

        return redirect()->back();
    }
}
