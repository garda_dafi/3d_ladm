<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Print3dRequest;
use App\Print3d;
use App\Page;
use File, Session;

class AdminPrint3dController extends Controller
{
    public function index()
    {
        $docs = Print3d::get();
        $page = Page::where('name', Print3d::name())->first();
        return view('admin.page.print3d.index', [
            'docs' => $docs,
            'page' => $page
        ]);
    }

    public function create()
    {
        return view('admin.page.form', [
            'data' => new Print3d,
            'page' => Print3d::name()
        ]);
    }

    public function store(Print3dRequest $request)
    {
        $print3d = new Print3d;

        $print3d->title = $request->title;
        $print3d->content_text = $request->content_text;
        $print3d->button_url = $request->button_url;
        $print3d->button_title = $request->button_title;
        $print3d->image_rotate = $request->image_rotate;

        if($request->image_url){
            $image_url                   = time().'.'.$request->image_url->getClientOriginalExtension();
            $request->image_url->move(public_path('images/print3d'), $image_url);
            $print3d->image_url               = $image_url;
        }

        if($print3d->save()) {
            Session::flash('success-saved');
            return redirect()->route('admin.print3d.index');
        }
    }

    public function edit($id)
    {
        $print3d = Print3d::find($id);

        return view('admin.page.form', [
            'data' => $print3d,
            'page' => Print3d::name()
        ]);
    }

    public function update(Print3dRequest $request, $id)
    {
        $print3d = Print3d::find($id);

        $print3d->title = $request->title;
        $print3d->content_text = $request->content_text;
        $print3d->button_url = $request->button_url;
        $print3d->button_title = $request->button_title;
        $print3d->image_rotate = $request->image_rotate;

        if($request->image_url){
            Session::flash('success-saved');
            File::delete(public_path('images/print3d/'.$print3d->image_url));
            $image_url                   = time().'.'.$request->image_url->getClientOriginalExtension();
            $request->image_url->move(public_path('images/print3d'), $image_url);
            $print3d->image_url               = $image_url;
        }

        if($print3d->save()) {
            return redirect()->route('admin.print3d.index');
        }
    }

    public function destroy($id)
    {
        $print3d = Print3d::find($id);

        if($print3d->delete()){
            File::delete(public_path('images/print3d/'.$print3d->image_url));
            Session::flash('success-delete');
            return redirect()->back();
        }

        return redirect()->back();
    }
}
