<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\HomeRequest;
use App\Home;
use App\Page;
use File, Session;

class AdminHomeController extends Controller
{
    public function index()
    {
        $docs = Home::get();
        $page = Page::where('name', Home::name())->first();

        return view('admin.page.home.index', [
            'docs' => $docs,
            'page' => $page
        ]);
    }

    public function create()
    {
        return view('admin.page.form', [
            'data' => new Home,
            'page' => Home::name()
        ]);
    }

    public function store(HomeRequest $request)
    {
        $home = new Home;

        $home->title = $request->title;
        $home->content_text = $request->content_text;
        $home->button_url = $request->button_url;
        $home->button_title = $request->button_title;
        $home->image_rotate = $request->image_rotate;

        if($request->image_url){
            $image_url                   = time().'.'.$request->image_url->getClientOriginalExtension();
            $request->image_url->move(public_path('images/home'), $image_url);
            $home->image_url               = $image_url;
        }

        if($home->save()) {
            Session::flash('success-saved');
            return redirect()->route('admin.home.index');
        }
    }

    public function edit($id)
    {
        $home = Home::find($id);

        return view('admin.page.form', [
            'data' => $home,
            'page' => Home::name()
        ]);
    }

    public function update(HomeRequest $request, $id)
    {
        $home = Home::find($id);

        $home->title = $request->title;
        $home->content_text = $request->content_text;
        $home->button_url = $request->button_url;
        $home->button_title = $request->button_title;
        $home->image_rotate = $request->image_rotate;

        if($request->image_url){
            File::delete(public_path('images/home/'.$home->image_url));
            $image_url                   = time().'.'.$request->image_url->getClientOriginalExtension();
            $request->image_url->move(public_path('images/home'), $image_url);
            $home->image_url               = $image_url;
        }

        if($home->save()) {
            return redirect()->route('admin.home.index');
        }
    }

    public function destroy($id)
    {
        $home = Home::find($id);

        if($home->delete()){
            File::delete(public_path('images/home/'.$home->image_url));
            Session::flash('success-delete');
            return redirect()->back();
        }

        return redirect()->back();
    }
}
