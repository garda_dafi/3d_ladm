<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\KjskbRequest;
use App\Kjskb;
use App\Page;
use File, Session;

class AdminKjskbController extends Controller
{
    public function index()
    {
        $docs = Kjskb::get();
        $page = Page::where('name', Kjskb::name())->first();
        return view('admin.page.kjskb.index', [
            'docs' => $docs,
            'page' => $page
        ]);
    }

    public function create()
    {
        return view('admin.page.form', [
            'data' => new Kjskb,
            'page' => Kjskb::name()
        ]);
    }

    public function store(KjskbRequest $request)
    {
        $kjskb = new Kjskb;

        $kjskb->title = $request->title;
        $kjskb->content_text = $request->content_text;
        $kjskb->button_url = $request->button_url;
        $kjskb->button_title = $request->button_title;
        $kjskb->image_rotate = $request->image_rotate;

        if($request->image_url){
            $image_url                   = time().'.'.$request->image_url->getClientOriginalExtension();
            $request->image_url->move(public_path('images/kjskb'), $image_url);
            $kjskb->image_url               = $image_url;
        }

        if($kjskb->save()) {
            Session::flash('success-saved');
            return redirect()->route('admin.kjskb.index');
        }
    }

    public function edit($id)
    {
        $kjskb = Kjskb::find($id);

        return view('admin.page.form', [
            'data' => $kjskb,
            'page' => Kjskb::name()
        ]);
    }

    public function update(KjskbRequest $request, $id)
    {
        $kjskb = Kjskb::find($id);

        $kjskb->title = $request->title;
        $kjskb->content_text = $request->content_text;
        $kjskb->button_url = $request->button_url;
        $kjskb->button_title = $request->button_title;
        $kjskb->image_rotate = $request->image_rotate;

        if($request->image_url){
            Session::flash('success-saved');
            File::delete(public_path('images/kjskb/'.$kjskb->image_url));
            $image_url                   = time().'.'.$request->image_url->getClientOriginalExtension();
            $request->image_url->move(public_path('images/kjskb'), $image_url);
            $kjskb->image_url               = $image_url;
        }

        if($kjskb->save()) {
            return redirect()->route('admin.kjskb.index');
        }
    }

    public function destroy($id)
    {
        $kjskb = Kjskb::find($id);

        if($kjskb->delete()){
            File::delete(public_path('images/kjskb/'.$kjskb->image_url));
            Session::flash('success-delete');
            return redirect()->back();
        }

        return redirect()->back();
    }
}
