<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\User;
use Session, Auth, File;
class AdminUserController extends Controller
{
    public function index()
    {
        return view('admin.user.index');
    }

    public function edit()
    {
        return view('admin.user.form');
    }

    public function update(UserRequest $request)
    {
        $user = Auth::user();

        $user->name = $request->name;
        $user->username = $request->username;
        if ($request->password) {
            $user->password = bcrypt($request->password);
            $user->email = $request->email;
        }

        if($request->avatar){
            File::delete(public_path('images/user/'.$user->avatar));
            $avatar                   = time().'.'.$request->avatar->getClientOriginalExtension();
            $request->avatar->move(public_path('images/user'), $avatar);
            $user->avatar               = $avatar;
        }

        if($user->save()) {
            Session::flash('success-saved');
            return redirect()->route('admin.user.index');
        }
    }
}
