<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CesiumRequest;
use App\Http\Requests\TokenRequest;
use App\Cesium;
use App\Page;
use App\User;
use File, Session, Auth;

class AdminCesiumController extends Controller
{
    public function index()
    {
        $docs = Cesium::get();
        $page = Page::where('name', Cesium::name())->first();

        return view('admin.page.cesium.index', [
            'docs' => $docs,
            'page' => $page
        ]);
    }

    public function create()
    {
        return view('admin.page.cesium.form', [
            'cesium' => new Cesium
        ]);
    }

    public function store(CesiumRequest $request)
    {
        $cesium = new Cesium;

        $cesium->title = $request->title;
        $cesium->script = $request->script;
        $cesium->viewer_name = $request->viewer_name;
        $cesium->viewer_option = $request->viewer_option;
        $cesium->description = $request->description;

        if($cesium->save()) {
            Session::flash('success-saved');
            return redirect()->route('admin.cesium.index');
        }
    }

    public function edit($id)
    {
        $cesium = Cesium::find($id);

        return view('admin.page.cesium.form', [
            'cesium' => $cesium
        ]);
    }

    public function update(CesiumRequest $request, $id)
    {
        $cesium = Cesium::find($id);

        $cesium->title = $request->title;
        $cesium->script = $request->script;
        $cesium->viewer_name = $request->viewer_name;
        $cesium->viewer_option = $request->viewer_option;
        $cesium->description = $request->description;

        if($cesium->save()) {
            return redirect()->route('admin.cesium.index');
        }
    }

    public function destroy($id)
    {
        $cesium = Cesium::find($id);

        if($cesium->delete()){
            File::delete(public_path('images/cesium/'.$cesium->image_url));
            Session::flash('success-delete');
            return redirect()->back();
        }

        return redirect()->back();
    }

    public function token()
    {
        return view('admin.page.cesium.token', [
            'page' => Page::where('name', Cesium::name())->first(),
            'user' => Auth::user()
        ]);
    }

    public function updateToken(TokenRequest $request)
    {
        $user = User::find(Auth::user()->id);
        $user->token = $request->token;

        if($user->save()){
            Session::flash('success-saved');
            return redirect()->route('admin.cesium.index');
        }
        Session::flash('error-saved');
        return redirect()->back();
    }
}
