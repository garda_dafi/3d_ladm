<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AdminRequest;
use App\About;
use App\Page;
use File, Session;

class AdminAboutController extends Controller
{
    public function index()
    {
        $docs = About::get();
        $page = Page::where('name', About::name())->first();
        return view('admin.page.about.index', [
            'docs' => $docs,
            'page' => $page
        ]);
    }

    public function create()
    {
        return view('admin.page.form', [
            'data' => new About,
            'page' => About::name()
        ]);
    }

    public function store(AboutRequest $request)
    {
        $about = new About;

        $about->title = $request->title;
        $about->content_text = $request->content_text;
        $about->button_url = $request->button_url;
        $about->button_title = $request->button_title;
        $about->image_rotate = $request->image_rotate;

        if($request->image_url){
            $image_url                   = time().'.'.$request->image_url->getClientOriginalExtension();
            $request->image_url->move(public_path('images/about'), $image_url);
            $about->image_url               = $image_url;
        }

        if($about->save()) {
            Session::flash('success-saved');
            return redirect()->route('admin.about.index');
        }
    }

    public function edit($id)
    {
        $about = About::find($id);

        return view('admin.page.form', [
            'data' => $about,
            'page' => About::name()
        ]);
    }

    public function update(AboutRequest $request, $id)
    {
        $about = About::find($id);

        $about->title = $request->title;
        $about->content_text = $request->content_text;
        $about->button_url = $request->button_url;
        $about->button_title = $request->button_title;
        $about->image_rotate = $request->image_rotate;

        if($request->image_url){
            Session::flash('success-saved');
            File::delete(public_path('images/about/'.$about->image_url));
            $image_url                   = time().'.'.$request->image_url->getClientOriginalExtension();
            $request->image_url->move(public_path('images/about'), $image_url);
            $about->image_url               = $image_url;
        }

        if($about->save()) {
            return redirect()->route('admin.about.index');
        }
    }

    public function destroy($id)
    {
        $about = About::find($id);

        if($about->delete()){
            File::delete(public_path('images/about/'.$about->image_url));
            Session::flash('success-delete');
            return redirect()->back();
        }

        return redirect()->back();
    }
}
