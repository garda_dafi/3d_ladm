<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $table = 'about';

    public static function page()
    {
        return 'About';
    }

    public static function name()
    {
        return 'about';
    }
}
