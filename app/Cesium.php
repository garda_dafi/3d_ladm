<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cesium extends Model
{
    protected $table = 'cesium';

    public static function page()
    {
        return 'Cesium';
    }

    public static function name()
    {
        return 'cesium';
    }
}
