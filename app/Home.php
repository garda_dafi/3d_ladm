<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    protected $table = 'home';

    public static function page()
    {
        return 'Home';
    }

    public static function name()
    {
        return 'home';
    }
}
