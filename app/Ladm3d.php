<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ladm3d extends Model
{
    protected $table = '3d_ladm';

    public static function page()
    {
        return '3D LADM';
    }

    public static function name()
    {
        return '3d_ladm';
    }
}
