<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Print3d extends Model
{
    protected $table = '3d_print';

    public static function page()
    {
        return '3D Print';
    }

    public static function name()
    {
        return '3d_print';
    }
}
