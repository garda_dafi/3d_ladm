<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kjskb extends Model
{
    protected $table = 'kjskb';

    public static function page()
    {
        return 'KJSKB';
    }

    public static function name()
    {
        return 'kjskb';
    }
}
