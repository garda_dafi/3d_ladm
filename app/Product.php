<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';

    public static function page()
    {
        return 'Product';
    }

    public static function name()
    {
        return 'product';
    }
}
