<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('page')->insert([
            'name' => App\Home::name(),
            'big_title' => 'BIG TITLE',
            'small_title' => 'Small Title'
        ]);

        DB::table('page')->insert([
            'name' => App\About::name(),
            'big_title' => 'BIG TITLE',
            'small_title' => 'Small Title'
        ]);

        DB::table('page')->insert([
            'name' => App\Print3d::name(),
            'big_title' => 'BIG TITLE',
            'small_title' => 'Small Title'
        ]);

        DB::table('page')->insert([
            'name' => App\Product::name(),
            'big_title' => 'BIG TITLE',
            'small_title' => 'Small Title'
        ]);

        DB::table('page')->insert([
            'name' => App\Kjskb::name(),
            'big_title' => 'BIG TITLE',
            'small_title' => 'Small Title'
        ]);

        DB::table('page')->insert([
            'name' => App\Ladm3d::name(),
            'big_title' => 'BIG TITLE',
            'small_title' => 'Small Title'
        ]);

        DB::table('page')->insert([
            'name' => App\Cesium::name(),
            'big_title' => '-',
            'small_title' => '-'
        ]);

        DB::table('page')->insert([
            'name' => App\User::name(),
            'big_title' => '-',
            'small_title' => '-'
        ]);
    }
}
