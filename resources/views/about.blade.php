<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/img/favicon.ico">

    <title>3D GEORICH</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/custom-animations.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

	<! -- ********** HEADER ********** -->
	<div id="h">
		<div class="container">
			<div class="row">
                @include('components.menu')
			    <div class="col-md-10 col-md-offset-1 mt">
			    	<h3>{{ $page->small_title }}</h3>
			    	<h1 class="mb">{{ $page->big_title }}</h1>
			    </div>
			    <div class="col-md-12 mt hidden-xs">
			    	<img src="assets/img/graph.png" class="img-responsive aligncenter" alt="" data-effect="slide-bottom">
			    </div>
			</div>
		</div><! --/container -->
	</div><! --/h -->

	<! -- ********** FIRST ********** -->
    @php
        $keyIndex = 0;
    @endphp
    @foreach ($docs as $data)
        @php
            if($keyIndex == 3) {
                $keyIndex = 1;
            }

            if($keyIndex == 0) $keyClass = 'w';
            if($keyIndex == 1) $keyClass = 'picton';
            if($keyIndex == 2) $keyClass = 'curious';
            if($keyIndex == 3) $keyClass = 'malibu';

            $keyIndex++;
        @endphp
    	<div class="{{ $keyClass }}">
    		<div class="row nopadding">
                @if ($keyClass == 'picton')
                    @if ($data->image_url)
                        <div class="col-md-6 pull-left">
                            <img src="{!! asset('images/home/'.$data->image_url) !!}" class="img-responsive alignright" alt="" data-effect="slide-right">
                        </div>
                    @endif

        			<div class="col-md-5 mt">
        				<h4>{{ $data->title }}</h4>
                        {!! $data->content_text !!}
        				<p class="mt"><a href="{{ $data->button_url }}" class="btn btn-info btn-theme">{{ $data->button_title }}</a></p>
        			</div>

                @else
        			<div class="col-md-5 col-md-offset-1 mt">
        				<h4>{{ $data->title }}</h4>
                        {!! $data->content_text !!}
        				<p class="mt"><a href="{{ $data->button_url }}" class="btn btn-info btn-theme">{{ $data->button_title }}</a></p>
        			</div>

                    @if ($data->image_url)
                        <div class="col-md-6 pull-right">
                            <img src="{!! asset('images/home/'.$data->image_url) !!}" class="img-responsive alignright" alt="" data-effect="slide-right">
                        </div>
                    @endif
                @endif

                @if ($data->is_text)
                    <div class="col-md-6 mt centered">
                        <h1 data-effect="slide-bottom">24</h1>
                        <h3>$ per month</h3>
                    </div>
                @endif

    		</div><! --/row -->
    	</div><! --/container -->
    @endforeach

	<! -- ********** CREDITS ********** -->
	<div id="c">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 centered">
					<p>Copyright 2014 | LandingSumo.com</p>
				</div>
			</div>
		</div><! --/container -->
	</div><! --/C -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/retina-1.1.0.js"></script>
    <script src="assets/js/jquery.unveilEffects.js"></script>
  </body>
</html>
