<nav class="navbar navbar-default bg-primary" style="background-color: #337ab7; color: #fff; padding: 15px">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="/">
        <img src="{{ asset('/assets/img/logogeo.png') }}" style="height: 55px; margin-right: 10px" alt="">
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a class="link-nav" href="{!! route('home') !!}">HOME</a></li>
        <li><a class="link-nav" href="{!! route('about') !!}">ABOUT</a></li>
        <li><a class="link-nav" href="{!! route('3d_ladm') !!}">3D LADM</a></li>
        <li><a class="link-nav" href="{!! route('3d_print') !!}">3D PRINT</a></li>
        <li><a class="link-nav" href="{!! route('product') !!}">OUR PRODUCT</a></li>
        <li><a class="link-nav" href="{!! route('kjskb') !!}">KJSKB KTS</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a class="link-nav" href="{!! route('login') !!}">SIGN IN</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
{{-- <div class="btn-group">
    <a href="{!! route('home') !!}" class="btn btn-lg btn-info"><span class="glyphicon glyphicon-home"></span> HOME</a>
    <a href="{!! route('about') !!}" class="btn btn-lg btn-info">ABOUT</a>
    <a href="{!! route('3d_ladm') !!}" class="btn btn-lg btn-info">3D LADM</a>
    <a href="{!! route('3d_print') !!}" class="btn btn-lg btn-info">3D PRINT</a>
    <a href="{!! route('product') !!}" class="btn btn-lg btn-info">OUR PRODUCT</a>
    <a href="{!! route('kjskb') !!}" class="btn btn-lg btn-info">KJSKB KTS</a>
    <a href="{{ route('login') }}" class="btn btn-lg btn-info">SIGN IN</a>
</div> --}}
