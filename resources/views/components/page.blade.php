<div class="col-md-10 col-md-offset-1 mt">
<h3>{{ $page->small_title }}</h3>
<h1 class="mb">{{ $page->big_title }}</h1>
</div>
<div class="col-md-12 mt hidden-xs">
  @if ($page->thumbnail)
    <img src="{{ asset('/images/page/thumbnail/'.$page->name.'/'.$page->thumbnail) }}" class="img-responsive aligncenter" style="height:350px" data-effect="slide-bottom">
  @else
    <img src="{{ asset('/assets/img/default.jpg') }}" class="img-responsive aligncenter" style="height:350px" data-effect="slide-bottom">
  @endif
</div>
