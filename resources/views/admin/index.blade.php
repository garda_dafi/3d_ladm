@extends('admin.layouts.main', [
  'page' => 'Dashboard'
])

@section('main')
<div class="container-fluid">
<div class="animated fadeIn">
  <!-- /.row-->
  <!-- /.card-->
  <div class="row">
    <div class="col-sm-6 col-lg-3">
      <a href="{!! route('admin.home.index') !!}" style="text-decoration: none; color:#000">
        <div class="brand-card">
          <div class="brand-card-header bg-twitter">
            <i class="fa fa-home"></i><br>
            <div class="chart-wrapper">
              <canvas id="social-box-chart-1" height="90"></canvas>
            </div>
          </div>
          <div class="brand-card-body">
            <div>
              <div class="text-value">{{ App\Home::page() }}</div>
            </div>
          </div>
        </div>
      </a>
    </div>
    <!-- /.col-->
    <div class="col-sm-6 col-lg-3">
      <a href="{!! route('admin.about.index') !!}" style="text-decoration: none; color:#000">
        <div class="brand-card">
          <div class="brand-card-header bg-twitter">
            <i class="fa fa-user"></i>
            <div class="chart-wrapper">
              <canvas id="social-box-chart-2" height="90"></canvas>
            </div>
          </div>
          <div class="brand-card-body">
            <div>
              <div class="text-value">{{ App\About::page() }}</div>
            </div>
          </div>
        </div>
      </a>
    </div>
    <!-- /.col-->
    <div class="col-sm-6 col-lg-3">
      <a href="{!! route('admin.3d_ladm.index') !!}" style="text-decoration: none; color:#000">
        <div class="brand-card">
          <div class="brand-card-header bg-twitter">
            <i class="fa fa-map"></i>
            <div class="chart-wrapper">
              <canvas id="social-box-chart-3" height="90"></canvas>
            </div>
          </div>
          <div class="brand-card-body">
            <div>
              <div class="text-value">{{ App\Ladm3d::page() }}</div>
            </div>
          </div>
        </div>
      </a>
    </div>
    <!-- /.col-->
    <div class="col-sm-6 col-lg-3">
      <a href="{!! route('admin.3d_print.index') !!}" style="text-decoration: none; color:#000">
        <div class="brand-card">
          <div class="brand-card-header bg-twitter">
            <i class="fa fa-print"></i>
            <div class="chart-wrapper">
              <canvas id="social-box-chart-4" height="90"></canvas>
            </div>
          </div>
          <div class="brand-card-body">
            <div>
              <div class="text-value">{{ App\Print3d::page() }}</div>
            </div>
          </div>
        </div>
      </a>
    </div>
    <!-- /.col-->
  </div>
  <div class="row">
    <div class="col-sm-6 col-lg-3">
      <a href="{!! route('admin.product.index') !!}" style="text-decoration: none; color:#000">
        <div class="brand-card">
          <div class="brand-card-header bg-twitter">
            <i class="fa fa-gift"></i>
            <div class="chart-wrapper">
              <canvas id="social-box-chart-4" height="90"></canvas>
            </div>
          </div>
          <div class="brand-card-body">
            <div>
              <div class="text-value">{{ App\Product::page() }}</div>
            </div>
          </div>
        </div>
      </a>
    </div>
    <div class="col-sm-6 col-lg-3">
      <a href="{!! route('admin.kjskb.index') !!}" style="text-decoration: none; color:#000">
        <div class="brand-card">
          <div class="brand-card-header bg-twitter">
            <i class="fa fa-graduation-cap"></i>
            <div class="chart-wrapper">
              <canvas id="social-box-chart-4" height="90"></canvas>
            </div>
          </div>
          <div class="brand-card-body">
            <div>
              <div class="text-value">{{ App\Kjskb::page() }}</div>
            </div>
          </div>
        </div>
      </a>
    </div>
    <div class="col-sm-6 col-lg-6">
      <a href="{!! route('admin.cesium.index') !!}" style="text-decoration: none; color:#000">
        <div class="brand-card">
          <div class="brand-card-header bg-facebook">
            <i class="fa fa-map"></i>
            <div class="chart-wrapper">
              <canvas id="social-box-chart-4" height="90"></canvas>
            </div>
          </div>
          <div class="brand-card-body">
            <div>
              <div class="text-value">{{ App\Cesium::page() }}</div>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>
  <!-- /.row-->
  <!-- /.row-->
</div>
</div>
@endsection
