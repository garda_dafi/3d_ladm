
  <nav class="sidebar-nav">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link {{ Request::is('/') ? 'active' : '' }}" href="{!! route('admin.dashboard.index') !!}">
          <i class="nav-icon icon-speedometer"></i> Dashboard
        </a>
      </li>
          <li class="nav-title">CONTENT</li>
          <li class="nav-item">
            <a class="nav-link {{ Request::is('*/home/*') ? 'active' : '' }}" href="{!! route('admin.home.index') !!}">
              <i class="nav-icon icon-home"></i> HOME</a>
          </li>
      <li class="nav-item">
        <a class="nav-link {{ Request::is('*/about/*') ? 'active' : '' }}" href="{!! route('admin.about.index') !!}">
          <i class="nav-icon icon-user"></i> ABOUT</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ Request::is('*/3d_ladm/*') ? 'active' : '' }}" href="{!! route('admin.3d_ladm.index') !!}">
          <i class="nav-icon icon-map"></i> 3D LADM</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ Request::is('*/3d_print/*') ? 'active' : '' }}" href="{!! route('admin.3d_print.index') !!}">
          <i class="nav-icon icon-printer"></i> 3D PRINT</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ Request::is('*/product/*') ? 'active' : '' }}" href="{!! route('admin.product.index') !!}">
          <i class="nav-icon fa fa-gift"></i> OUR PRODUCT</a>
      </li>
      <li class="nav-item">
        <a class="nav-link {{ Request::is('*/kjskb/*') ? 'active' : '' }}" href="{!! route('admin.kjskb.index') !!}">
          <i class="nav-icon fa fa-graduation-cap"></i> KJSKB KTS</a>
      </li>
      <li class="nav-title">MAP</li>
      <li class="nav-item">
        <a class="nav-link {{ Request::is('*/cesium/*') ? 'active' : '' }}" href="{!! route('admin.cesium.index') !!}">
          <i class="nav-icon icon-map"></i> CESIUM</a>
      </li>
      <li class="divider"></li>
    </ul>
  </nav>
  <button class="sidebar-minimizer brand-minimizer" type="button"></button>
