<div class="row">
    <div class="col-sm-12 col-xl-12">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-align-justify"></i> {{ $page->name }}
          <small>Header</small>
        </div>
        <div class="card-body">
          <div class="jumbotron jumbotron-fluid" style="background-image: url('/images/page/background/{{ $page->name }}/{{ $page->background }}'); background-size: cover">
            <div class="container text-center" style="color: #fff">
                <p class="lead">{{ $page->small_title }}</p>
              <h1 class="display-3">{{ $page->big_title }}</h1>
              @if ($page->thumbnail)
                  <img src="/images/page/thumbnail/{{ $page->name }}/{{ $page->thumbnail }}" height="100px" />
              @else
                  <img src="/assets/img/default.jpg" height="100px" />
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
