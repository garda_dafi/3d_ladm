
  <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="{!! route('admin.dashboard.index') !!}">
    <img src="{{ asset('/assets/img/logogeo.png') }}" style="height: 40px;" alt="">
    {{-- <h5> 3D GEORICH</h5> --}}
    {{-- <img class="navbar-brand-full" src="{{ asset('/assets/admin/img/brand/logo.svg') }}" width="89" height="25" alt="CoreUI Logo">
    <img class="navbar-brand-minimized" src="{{ asset('/assets/admin/img/brand/sygnet.svg') }}" width="30" height="30" alt="CoreUI Logo"> --}}
  </a>
  <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
    <span class="navbar-toggler-icon"></span>
  </button>
  <ul class="nav navbar-nav d-md-down-none">
      <li class="nav-item"><a class="nav-link" target="_blank" href="{!! route('home') !!}"><span class="fa fa-link"></span> View Web</a></li>
  </ul>
  <ul class="nav navbar-nav ml-auto">
    <li class="nav-item dropdown">
      <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          {{ Auth::user()->name }}
        <img class="img-avatar" src="{{ Auth::user()->avatar ? asset('/images/user/'.Auth::user()->avatar) : asset('/assets/admin/avatar/default.jpg') }}" alt="admin@bootstrapmaster.com">
      </a>
      <div class="dropdown-menu dropdown-menu-right">
        <div class="dropdown-header text-center">
          <strong>Account</strong>
        </div>
        <a class="dropdown-item" href="{!! route('admin.user.index') !!}">
          <i class="fa fa-user"></i> Profile</a>
        <a class="dropdown-item" href="{!! route('admin.user.edit') !!}">
          <i class="fa fa-cog"></i> Setting</a>
        <a class="dropdown-item" href="{!! route('logout') !!}">
          <i class="fa fa-lock"></i> Logout</a>
      </div>
    </li>
  </ul>
  {{-- <button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
    <span class="navbar-toggler-icon"></span>
  </button>
  <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
    <span class="navbar-toggler-icon"></span>
  </button> --}}
