<!DOCTYPE html>
<!--
* CoreUI - Free Bootstrap Admin Template
* @version v2.1.12
* @link https://coreui.io
* Copyright (c) 2018 creativeLabs Łukasz Holeczek
* Licensed under MIT (https://coreui.io/license)
-->

<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <link rel="icon" href="{{ asset('/assets/img/logogeo.png') }}">
    <title>3D LADM | Admin</title>
    <!-- Icons-->
    <link href="{{ asset('/assets/admin/vendors/@coreui/icons/css/coreui-icons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/admin/vendors/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/admin/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/admin/vendors/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="{{ asset('/assets/admin/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/admin/vendors/pace-progress/css/pace.min.css') }}" rel="stylesheet">
    @yield('styles')
    <!-- Global site tag (gtag.js) - Google Analytics-->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-118965717-3"></script>
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      // Shared ID
      gtag('config', 'UA-118965717-3');
      // Bootstrap ID
      gtag('config', 'UA-118965717-5');
    </script>
  </head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    <header class="app-header navbar">
        @include('admin.components.header')
    </header>
    <div class="app-body">
      <div class="sidebar">
          @include('admin.components.sidebar')
      </div>
      <main class="main">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="/admin">Admin</a>
            </li>
            <li class="breadcrumb-item active">{{ $page }}</li>
            <!-- Breadcrumb Menu-->
            {{-- <li class="breadcrumb-menu d-md-down-none">
              <div class="btn-group" role="group" aria-label="Button group">
                <a class="btn" href="#">
                  <i class="icon-speech"></i>
                </a>
                <a class="btn" href="./">
                  <i class="icon-graph"></i>  Dashboard</a>
                <a class="btn" href="#">
                  <i class="icon-settings"></i>  Settings</a>
              </div>
            </li> --}}
          </ol>
          @if (Session::has('success-delete'))
              <div class="alert alert-success">
                  Data has been successfully deleted
              </div>
          @endif
          @if (Session::has('error-delete'))
              <div class="alert alert-danger">
                  Data failed to delete
              </div>
          @endif
          @if (Session::has('success-saved'))
              <div class="alert alert-success">
                  Data has been successfully saved
              </div>
          @endif
          @if (Session::has('error-saved'))
              <div class="alert alert-danger">
                  Data failed to save
              </div>
          @endif
          @yield('main')
      </main>
      <aside class="aside-menu">
          @include('admin.components.aside-menu')
      </aside>
    </div>
    <footer class="app-footer">
        @include('admin.components.footer')
    </footer>
    <!-- CoreUI and necessary plugins-->
    <script src="{{ asset('/assets/admin/vendors/jquery/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/assets/admin/vendors/popper.js/js/popper.min.js') }}"></script>
    <script src="{{ asset('/assets/admin/vendors/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/assets/admin/vendors/pace-progress/js/pace.min.js') }}"></script>
    <script src="{{ asset('/assets/admin/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('/assets/admin/vendors/@coreui/coreui/js/coreui.min.js') }}"></script>
    <!-- Plugins and scripts required by this view-->
    {{-- <script src="{{ asset('/assets/admin/vendors/chart.js/js/Chart.min.js') }}"></script> --}}
    <script src="{{ asset('/assets/admin/vendors/@coreui/coreui-plugin-chartjs-custom-tooltips/js/custom-tooltips.min.js') }}"></script>
    {{-- <script src="{{ asset('/assets/admin/js/main.js') }}"></script> --}}
    @yield('scripts')
  </body>
</html>
