@extends('admin.layouts.main', [
    'page' => App\Ladm3d::page()
])

@section('styles')
    <link href="{{ asset('/assets/vendors/dropify/css/dropify.min.css') }}" rel="stylesheet">
@endsection

@section('main')<!-- Breadcrumb-->
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                      <div class="card">
                      <form class="form-horizontal" action="{!! Request::route()->named('admin.3d_ladm.create') ? route('admin.3d_ladm.store') : route('admin.3d_ladm.update', $ladm3d->id) !!}" method="post" enctype="multipart/form-data">
                        <div class="card-header">
                          <strong>{{ App\Ladm3d::page() }}</strong></div>
                        <div class="card-body">
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="text-input">Title</label>
                              <div class="col-md-9">
                                <input class="form-control" id="text-input" type="text" name="title" placeholder="Title" value="{{ old('title', $ladm3d->title) }}">
                                @if ($errors->has('title'))
                                    <span class="help-block text-danger">{{ $errors->first('title') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="textarea-input">Content Text</label>
                              <div class="col-md-9">
                                <textarea id="textContent" name="content_text" rows="9" placeholder="Content Text ...">{{ old('content_text', $ladm3d->content_text) }}</textarea>
                                @if ($errors->has('content_text'))
                                    <span class="help-block text-danger">{{ $errors->first('content_text') }}</span>
                                @endif
                              </div>
                            </div>
                            {{-- <div class="form-group row">
                              <label class="col-md-3 col-form-label">Radios</label>
                              <div class="col-md-9 col-form-label">
                                <div class="form-check">
                                  <input class="form-check-input" id="radio1" type="radio" value="radio1" name="radios">
                                  <label class="form-check-label" for="radio1">Image</label>
                                </div>
                                <div class="form-check">
                                  <input class="form-check-input" id="radio2" type="radio" value="radio2" name="radios">
                                  <label class="form-check-label" for="radio2">Text</label>
                                </div>
                              </div>
                            </div> --}}
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="text-input">Button Title</label>
                              <div class="col-md-9">
                                <input class="form-control" id="text-input" type="text" name="button_title" placeholder="Button Title" value="{{ old('button_title', $ladm3d->button_title) }}">
                                @if ($errors->has('button_title'))
                                    <span class="help-block text-danger">{{ $errors->first('button_title') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="text-input">Button URL</label>
                              <div class="col-md-9">
                                <input class="form-control" id="text-input" type="text" name="button_url" placeholder="http://" value="{{ old('button_url', $ladm3d->button_url) }}">
                                @if ($errors->has('button_url'))
                                    <span class="help-block text-danger">{{ $errors->first('button_url') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="file-input">Image</label>
                              <div class="col-md-4">
                                <input id="file-input" class="dropify" type="file" name="image_url" data-default-file="{!! ($ladm3d->image_url) ? asset('images/ladm3d/'.$ladm3d->image_url): '' !!}">
                                @if ($errors->has('image_url'))
                                    <span class="help-block text-danger">{{ $errors->first('image_url') }}</span>
                                @endif
                              </div>
                            </div>
                        </div>
                        <div class="card-footer">
                          <button class="btn btn-sm btn-primary" type="submit">
                              {{ csrf_field() }}
                            <i class="fa fa-dot-circle-o"></i> Submit</button>
                          <button class="btn btn-sm btn-danger" type="reset">
                            <i class="fa fa-ban"></i> Reset</button>
                        </div>
                        </form>
                      </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('/assets/vendors/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ asset('/assets/vendors/tinymce/js/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            tinymce.init({
                selector: '#textContent',
                height: 300
            });
            $('.dropify').dropify();
        });
    </script>
@endsection
