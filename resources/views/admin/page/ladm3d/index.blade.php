@extends('admin.layouts.main', [
    'page' => App\Ladm3d::page()
])

@section('main')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="btn-group">
                        <a class="btn btn-success" style="margin-bottom: 10px" href="{!! route('admin.cesium.create') !!}"><span class="fa fa-plus"></span> Add CESIUM</a>
                        <a class="btn btn-primary" style="margin-bottom: 10px" href="{!! route('admin.page.edit', App\Ladm3d::name()) !!}"><span class="fa fa-edit"></span> Edit Header</a>
                    </div>
                </div>
            </div>
            @include('admin.components.page')
            <div class="row">
                @foreach ($docs as $data)
                    <div class="col-sm-12 col-md-12">
                        <div class="card card-accent-primary">
                            <div class="card-header">{{ $data->title }}</div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        {!! $data->content_text !!}
                                        <a class="btn btn-primary" href="{{ $data->button_url }}">{{ $data->button_title }}</a>
                                    </div>
                                    <div class="col-md-4">
                                        <img src="{!! asset('images/3d_ladm/'.$data->image_url) !!}" width="100%" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="btn-group pull-right">
                                    <a class="btn btn-sm btn-info" style="color:#fff" href="{!! route('admin.3d_ladm.edit', $data->id) !!}">Edit</a>
                                    <a class="btn btn-sm btn-danger" href="{!! route('admin.3d_ladm.destroy', $data->id) !!}">Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
