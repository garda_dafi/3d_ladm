@extends('admin.layouts.main', [
    'page' => App\Cesium::page()
])

@section('styles')
    <link rel="stylesheet" href="{!! asset('/assets/js/Cesium/Widgets/widgets.css') !!}">
    <link rel="stylesheet" href="{!! asset('/assets/js/highlight/styles/default.css') !!}">
@endsection

@section('main')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="btn-group">
                        <a class="btn btn-success" style="margin-bottom: 10px" href="{!! route('admin.cesium.create') !!}"><span class="fa fa-map"></span> Add Map</a>
                        <a class="btn btn-primary" style="margin-bottom: 10px" href="{!! route('admin.cesium.token') !!}"><span class="fa fa-edit"></span> Edit Token</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h5>Token: <small>{{ Auth::user()->token }}</small></h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach ($docs as $data)
                    <div class="col-sm-12 col-md-12">
                        <div class="card card-accent-primary">
                            <div class="card-header">{{ $data->title }}</div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        {!! $data->description !!}
                                        <pre>
                                            <code class="JavaScript">var viewer = new Cesium.Viewer('{!! $data->viewer_name !!}',{!! $data->viewer_option ? $data->viewer_option : '{}' !!});
{!! $data->script !!}
                                            </code>
                                        </pre>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="{{ $data->viewer_name }}"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="btn-group pull-right">
                                    <a class="btn btn-sm btn-info" style="color:#fff" href="{!! route('admin.cesium.edit', $data->id) !!}">Edit</a>
                                    <a class="btn btn-sm btn-danger" href="{!! route('admin.cesium.destroy', $data->id) !!}">Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{!! asset('/assets/js/Cesium/Cesium.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/assets/js/highlight/highlight.pack.js') !!}"></script>
    <script>hljs.initHighlightingOnLoad();</script>
    <script type="text/javascript">
        Cesium.Ion.defaultAccessToken = "{{ Auth::user()->token }}";
        @foreach ($docs as $data)
            (function() {
                var viewer = new Cesium.Viewer('{!! $data->viewer_name !!}',{!! $data->viewer_option ? $data->viewer_option : '{}' !!});
                {!! $data->script !!};
            })(this);
        @endforeach

    </script>
@endsection
