@extends('admin.layouts.main', [
    'page' => App\Cesium::page()
])

@section('styles')
    <link href="{{ asset('/assets/vendors/dropify/css/dropify.min.css') }}" rel="stylesheet">
    <style type="text/css" media="screen">
        #editor1 {
            height: 100px;
        }
        #editor2 {
            height: 200px;
        }
    </style>
@endsection

@section('main')<!-- Breadcrumb-->
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                      <div class="card">
                      <form class="form-horizontal" action="{!! Request::route()->named('admin.cesium.create') ? route('admin.cesium.store') : route('admin.cesium.update', $cesium->id) !!}" method="post" enctype="multipart/form-data">
                        <div class="card-header">
                          <strong>{{ App\Cesium::page() }}</strong></div>
                        <div class="card-body">
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="text-input">Title</label>
                              <div class="col-md-9">
                                <input class="form-control" id="text-input" type="text" name="title" placeholder="Title" value="{{ old('title', $cesium->title) }}">
                                @if ($errors->has('title'))
                                    <span class="help-block text-danger">{{ $errors->first('title') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="text-input">Viewer Name</label>
                              <div class="col-md-9">
                                <input class="form-control" id="text-input" type="text" name="viewer_name" placeholder="Container Name" value="{{ old('viewer_name', $cesium->viewer_name) }}">
                                @if ($errors->has('viewer_name'))
                                    <span class="help-block text-danger">{{ $errors->first('viewer_name') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="textarea-input">Viewer Option</label>
                              <div class="col-md-9">
                                <div id="editor1">{{ old('viewer_option', $cesium->viewer_option) }}</div>
                                @if ($errors->has('viewer_option'))
                                    <span class="help-block text-danger">{{ $errors->first('viewer_option') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="textarea-input">Script</label>
                              <div class="col-md-9">
                                <div id="editor2">{{ old('script', $cesium->script) }}</div>
                                @if ($errors->has('script'))
                                    <span class="help-block text-danger">{{ $errors->first('script') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="textarea-input">Description</label>
                              <div class="col-md-9">
                                <textarea id="textContent" name="description" rows="9" placeholder="Content Text ...">{{ old('description', $cesium->description) }}</textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block text-danger">{{ $errors->first('description') }}</span>
                                @endif
                              </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input id="script" type="hidden" name="script" value="{{ old('script', $cesium->script) }}">
                            <input id="viewer_option" type="hidden" name="viewer_option" value="{{ old('viewer_option', $cesium->viewer_option) }}">
                          <button class="btn btn-sm btn-primary" type="submit">
                              {{ csrf_field() }}
                            <i class="fa fa-dot-circle-o"></i> Submit</button>
                          <button class="btn btn-sm btn-danger" type="reset">
                            <i class="fa fa-ban"></i> Reset</button>
                        </div>
                        </form>
                      </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('/assets/vendors/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ asset('/assets/js/ace/ace.js') }}"></script>
    <script src="{{ asset('/assets/vendors/tinymce/js/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            tinymce.init({
                selector: '#textContent',
                height: 300
            });
            $('.dropify').dropify();

            var viewerOption = ace.edit("editor1");
            viewerOption.setTheme("ace/theme/textmate");
            viewerOption.session.setMode("ace/mode/javascript");
            var optionScript = $('#viewer_option');
            viewerOption.getSession().setValue(optionScript.val());
            viewerOption.getSession().on('change', function(){
              optionScript.val(viewerOption.getSession().getValue());
            });

            var editor = ace.edit("editor2");
            editor.setTheme("ace/theme/textmate");
            editor.session.setMode("ace/mode/javascript");
            var inputScript = $('#script');
            editor.getSession().setValue(inputScript.val());
            editor.getSession().on('change', function(){
              inputScript.val(editor.getSession().getValue());
            });

        });
    </script>
@endsection
