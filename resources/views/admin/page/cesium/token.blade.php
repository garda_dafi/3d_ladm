@extends('admin.layouts.main', [
    'page' => App\Cesium::page()
])

@section('styles')
    <link href="{{ asset('/assets/vendors/dropify/css/dropify.min.css') }}" rel="stylesheet">
@endsection

@section('main')<!-- Breadcrumb-->
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                      <div class="card">
                      <form class="form-horizontal" action="{!! route('admin.cesium.updateToken', $page->name) !!}" method="post" enctype="multipart/form-data">
                        <div class="card-header">
                          <strong>{{ App\Cesium::page() }}</strong></div>
                        <div class="card-body">
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="text-input">Token</label>
                              <div class="col-md-9">
                                <textarea class="form-control" id="text-input" type="text" name="token" placeholder="Token">{{ old('token', $user->token) }}</textarea>
                                @if ($errors->has('token'))
                                    <span class="help-block text-danger">Required</span>
                                @endif
                              </div>
                            </div>
                        </div>
                        <div class="card-footer">
                          <button class="btn btn-sm btn-primary" type="submit">
                              {{ csrf_field() }}
                            <i class="fa fa-dot-circle-o"></i> Submit</button>
                          <button class="btn btn-sm btn-danger" type="reset">
                            <i class="fa fa-ban"></i> Reset</button>
                        </div>
                        </form>
                      </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('/assets/vendors/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ asset('/assets/vendors/tinymce/js/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            tinymce.init({
                selector: '#textContent',
                height: 300
            });
            $('.dropify').dropify();
        });
    </script>
@endsection
