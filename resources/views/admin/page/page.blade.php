@extends('admin.layouts.main', [
    'page' => App\Home::page()
])

@section('styles')
    <link href="{{ asset('/assets/vendors/dropify/css/dropify.min.css') }}" rel="stylesheet">
@endsection

@section('main')<!-- Breadcrumb-->
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                      <div class="card">
                      <form class="form-horizontal" action="{!! route('admin.page.update', $page->name) !!}" method="post" enctype="multipart/form-data">
                        <div class="card-header">
                          <strong>Home</strong> Page</div>
                        <div class="card-body">
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="text-input">Big Title</label>
                              <div class="col-md-9">
                                <input class="form-control" id="text-input" type="text" name="big_title" placeholder="Title" value="{{ old('big_title', $page->big_title) }}">
                                @if ($errors->has('big_title'))
                                    <span class="help-block text-danger">{{ $errors->first('big_title') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="text-input">Small Title</label>
                              <div class="col-md-9">
                                <input class="form-control" id="text-input" type="text" name="small_title" placeholder="Title" value="{{ old('small_title', $page->small_title) }}">
                                @if ($errors->has('small_title'))
                                    <span class="help-block text-danger">{{ $errors->first('small_title') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="file-input">Thumbnail</label>
                              <div class="col-md-4">
                                <input id="file-input" class="dropify" type="file" name="thumbnail" data-default-file="{!! ($page->thumbnail) ? asset('images/page/thumbnail/'.$page->name.'/'.$page->thumbnail): '' !!}">
                                @if ($errors->has('thumbnail'))
                                    <span class="help-block text-danger">{{ $errors->first('thumbnail') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="file-input">Background</label>
                              <div class="col-md-4">
                                <input id="file-input" class="dropify" type="file" name="background" data-default-file="{!! ($page->background) ? asset('images/page/background/'.$page->name.'/'.$page->background): '' !!}">
                                @if ($errors->has('background'))
                                    <span class="help-block text-danger">{{ $errors->first('background') }}</span>
                                @endif
                              </div>
                            </div>
                        </div>
                        <div class="card-footer">
                          <button class="btn btn-sm btn-primary" type="submit">
                              {{ csrf_field() }}
                            <i class="fa fa-dot-circle-o"></i> Submit</button>
                          <button class="btn btn-sm btn-danger" type="reset">
                            <i class="fa fa-ban"></i> Reset</button>
                        </div>
                        </form>
                      </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('/assets/vendors/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ asset('/assets/vendors/tinymce/js/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            tinymce.init({
                selector: '#textContent',
                height: 300
            });
            $('.dropify').dropify();
        });
    </script>
@endsection
