@extends('admin.layouts.main', [
    'page' => App\Home::page()
])

@section('main')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-6 offset-md-3">
                    <div class="card card-accent-primary text-center">
                        <div class="card-header">{{ Auth::user()->name }}</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    @if (Auth::user()->avatar)
                                        <img class="img-avatar" src="{!! asset('images/user/'.Auth::user()->avatar) !!}" style="height:100px" alt="">
                                    @else
                                        <img class="img-avatar" src="{!! asset('/assets/admin/avatar/default.jpg') !!}" style="height:100px" alt="">
                                    @endif
                                    <br><br>
                                    <small>Username:</small> <br>
                                    {!! Auth::user()->username !!} <br>
                                    <small>Email</small> <br>
                                    {!! Auth::user()->email !!}
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                                <a class="btn btn-sm btn-info" style="color:#fff" href="{!! route('admin.user.edit') !!}">Edit</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
