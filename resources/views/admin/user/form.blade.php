@extends('admin.layouts.main', [
    'page' => 'User'
])

@section('styles')
    <link href="{{ asset('/assets/vendors/dropify/css/dropify.min.css') }}" rel="stylesheet">
@endsection

@section('main')<!-- Breadcrumb-->
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                      <div class="card">
                      <form class="form-horizontal" action="{!! route('admin.user.update') !!}" method="post" enctype="multipart/form-data">
                        <div class="card-header">
                          <strong>User</strong></div>
                        <div class="card-body">
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="text-input">Name</label>
                              <div class="col-md-9">
                                <input class="form-control" id="text-input" type="text" name="name" placeholder="Name" value="{{ old('name', Auth::user()->name) }}">
                                @if ($errors->has('name'))
                                    <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="text-input">Username</label>
                              <div class="col-md-9">
                                <input class="form-control" id="text-input" type="text" name="username" placeholder="Username" value="{{ old('username', Auth::user()->username) }}">
                                @if ($errors->has('username'))
                                    <span class="help-block text-danger">{{ $errors->first('username') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="text-input">Password</label>
                              <div class="col-md-9">
                                <input class="form-control" id="text-input" type="password" name="password" placeholder="Password" value="">
                                @if ($errors->has('password'))
                                    <span class="help-block text-danger">{{ $errors->first('password') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="text-input">Password Confirmation</label>
                              <div class="col-md-9">
                                <input class="form-control" id="text-input" type="password" name="password_confirmation" placeholder="Password Confirmation" value="">
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block text-danger">{{ $errors->first('password_confirmation') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="text-input">Email</label>
                              <div class="col-md-9">
                                <input class="form-control" id="text-input" type="text" name="email" placeholder="Email" value="{{ old('email', Auth::user()->email) }}">
                                @if ($errors->has('email'))
                                    <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                                @endif
                              </div>
                            </div>
                            <div class="form-group row">
                              <label class="col-md-3 col-form-label" for="file-input">Avatar</label>
                              <div class="col-md-6">
                                <input id="file-input" class="dropify" type="file" name="avatar" data-default-file="{!! (Auth::user()->avatar) ? asset('images/user/'.Auth::user()->avatar): '' !!}">
                                @if ($errors->has('avatar'))
                                    <span class="help-block text-danger">{{ $errors->first('avatar') }}</span>
                                @endif
                              </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                          <button class="btn btn-sm btn-primary" type="submit">
                              {{ csrf_field() }}
                            <i class="fa fa-dot-circle-o"></i> Submit</button>
                          <button class="btn btn-sm btn-danger" type="reset">
                            <i class="fa fa-ban"></i> Reset</button>
                        </div>
                        </form>
                      </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('/assets/vendors/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ asset('/assets/vendors/tinymce/js/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            tinymce.init({
                selector: '#textContent',
                height: 300
            });
            $('.dropify').dropify();
        });
    </script>
@endsection
