<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="PT. GEOSPASIAL RESEARCH 3D (3D GEORICH)" content="">
    <meta name="33dgeorich" content="">
    <link rel="stylesheet" href="{!! asset('/assets/js/Cesium/Widgets/widgets.css') !!}">
    <link rel="icon" href="{{ asset('/assets/img/logogeo.png') }}">

    <title>3D GEORICH</title>
    <!-- Bootstrap core CSS -->
    <link href="/assets/vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/custom-animations.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style media="screen">
      .link-nav {
        color: #fff !important;
      }
      .link-nav:hover {
          background-color: #fff !important;
          color: #337ab7 !important;
      }
    </style>
  </head>

  <body>

	<! -- ********** HEADER ********** -->
	<div id="h" style="background-image: url('/images/page/background/{{ $page->name }}/{{ $page->background }}'); background-size: cover">
		<div class="container">
			<div class="row">
                @include('components.page');
			</div>
		</div><! --/container -->
	</div><! --/h -->
    @include('components.menu')

	<! -- ********** FIRST ********** -->
    @php
        $keyIndex = 0;
    @endphp
    @foreach ($docs as $data)
        @php
            if($keyIndex == 3) {
                $keyIndex = 1;
            }

            if($keyIndex == 0) $keyClass = 'w';
            if($keyIndex == 1) $keyClass = 'picton';
            if($keyIndex == 2) $keyClass = 'curious';
            if($keyIndex == 3) $keyClass = 'malibu';

            $keyIndex++;
        @endphp
    	<div class="{{ $keyClass }}">
    		<div class="row nopadding">
                @if ($keyClass == 'picton')
                    <div class="col-md-6 pull-left">
                        <div id="{{ $data->viewer_name }}" data-effect="slide-left"></div>
                    </div>

        			<div class="col-md-5 mt">
        				<h4>{{ $data->title }}</h4>
                        {!! $data->description !!}
        			</div>

                @else
        			<div class="col-md-5 col-md-offset-1 mt">
        				<h4>{{ $data->title }}</h4>
                        {!! $data->description !!}
        			</div>

                    <div class="col-md-6 pull-right">
                        <div id="{{ $data->viewer_name }}" data-effect="slide-right"></div>
                    </div>
                @endif

    		</div><! --/row -->
    	</div><! --/container -->
    @endforeach

	<! -- ********** CREDITS ********** -->
	<div id="c">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 centered">
					<p>&copy; 2019 | PT. GEOSPASIAL RESEARCH 3D (3D GEORICH)</p>
				</div>
			</div>
		</div><! --/container -->
	</div><! --/C -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/retina-1.1.0.js"></script>
    <script src="assets/js/jquery.unveilEffects.js"></script>
    <script type="text/javascript" src="{!! asset('/assets/js/Cesium/Cesium.js') !!}"></script>
    <script type="text/javascript">
        Cesium.Ion.defaultAccessToken = "{{ App\User::find(1)->token }}";

        @foreach ($docs as $data)
            (function() {
                @if($data->viewer_option)
                var option = {{ $data->viewer_option }};
                option['fullscreenElement'] = {{ $data->viewer_name }};
                @endif
                var viewer = new Cesium.Viewer('{!! $data->viewer_name !!}',{!! $data->viewer_option ? 'option' : '{ fullscreenElement : "'.$data->viewer_name.'" }' !!});
                {!! $data->script !!};
            })(this);
        @endforeach
    </script>
  </body>
</html>
