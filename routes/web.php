<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Requests\UserRegisterRequest;
use App\User;

Route::get('/', function () {
    $docs = App\Home::get();
    $page = App\Page::where('name', App\Home::name())->first();
    return view('front', ['docs' => $docs, 'page' => $page]);
})->name('home');

Route::get('/about', function () {
    $docs = App\About::get();
    $page = App\Page::where('name', 'home')->first();
    return view('front', ['docs' => $docs, 'page' => $page]);
})->name('about');

Route::get('/cesium', function () {
    $docs = App\Cesium::get();
    $page = App\Page::where('name', App\Cesium::name())->first();
    return view('cesium', ['docs' => $docs, 'page' => $page]);
})->name('cesium');

Route::get('/3d_ladm', function () {
    $docs = App\Cesium::get();
    $page = App\Page::where('name',  App\Ladm3d::name())->first();
    return view('3d_ladm', ['docs' => $docs, 'page' => $page]);
})->name('3d_ladm');

Route::get('/3d_print', function () {
    $docs = App\Print3d::get();
    $page = App\Page::where('name', App\Print3d::name())->first();
    return view('front', ['docs' => $docs, 'page' => $page]);
})->name('3d_print');

Route::get('/product', function () {
    $docs = App\Product::get();
    $page = App\Page::where('name', App\Product::name())->first();
    return view('front', ['docs' => $docs, 'page' => $page]);
})->name('product');

Route::get('/kjskb', function () {
    $docs = App\Kjskb::get();
    $page = App\Page::where('name', App\Kjskb::name())->first();
    return view('front', ['docs' => $docs, 'page' => $page]);
})->name('kjskb');

Route::prefix('admin')->middleware('auth')->group(function(){
    Route::get('/', 'AdminController@index')->name('admin.dashboard.index');

    Route::get('/home', 'AdminHomeController@index')->name('admin.home.index');
    Route::get('/home/create', 'AdminHomeController@create')->name('admin.home.create');
    Route::post('/home', 'AdminHomeController@store')->name('admin.home.store');
    Route::get('/home/{id}/edit', 'AdminHomeController@edit')->name('admin.home.edit');
    Route::post('/home/{id}', 'AdminHomeController@update')->name('admin.home.update');
    Route::get('/home/{id}/destroy', 'AdminHomeController@destroy')->name('admin.home.destroy');

    Route::get('/about', 'AdminAboutController@index')->name('admin.about.index');
    Route::get('/about/create', 'AdminAboutController@create')->name('admin.about.create');
    Route::post('/about', 'AdminAboutController@store')->name('admin.about.store');
    Route::get('/about/{id}/edit', 'AdminAboutController@edit')->name('admin.about.edit');
    Route::post('/about/{id}', 'AdminAboutController@update')->name('admin.about.update');
    Route::get('/about/{id}/destroy', 'AdminAboutController@destroy')->name('admin.about.destroy');

    Route::get('/3d_ladm', 'AdminLadm3dController@3d_ladm')->name('admin.3d_ladm.index');
    Route::get('/3d_ladm', 'AdminLadm3dController@index')->name('admin.3d_ladm.index');
    Route::get('/3d_ladm/create', 'AdminLadm3dController@create')->name('admin.3d_ladm.create');
    Route::post('/3d_ladm', 'AdminLadm3dController@store')->name('admin.3d_ladm.store');
    Route::get('/3d_ladm/{id}/edit', 'AdminLadm3dController@edit')->name('admin.3d_ladm.edit');
    Route::post('/33d_ladm/{id}', 'AdminLadm3dController@update')->name('admin.33d_ladm.update');
    Route::get('/3d_ladm/{id}/destroy', 'AdminLadm3dController@destroy')->name('admin.3d_ladm.destroy');

    Route::get('/3d_print', 'AdminPrint3dController@3d_print')->name('admin.3d_print.index');
    Route::get('/3d_print', 'AdminPrint3dController@index')->name('admin.3d_print.index');
    Route::get('/3d_print/create', 'AdminPrint3dController@create')->name('admin.3d_print.create');
    Route::post('/3d_print', 'AdminPrint3dController@store')->name('admin.3d_print.store');
    Route::get('/3d_print/{id}/edit', 'AdminPrint3dController@edit')->name('admin.3d_print.edit');
    Route::post('/3d_print/{id}', 'AdminPrint3dController@update')->name('admin.3d_print.update');
    Route::get('/3d_print/{id}/destroy', 'AdminPrint3dController@destroy')->name('admin.3d_print.destroy');

    Route::get('/product', 'AdminProductController@product')->name('admin.product.index');
    Route::get('/product', 'AdminProductController@index')->name('admin.product.index');
    Route::get('/product/create', 'AdminProductController@create')->name('admin.product.create');
    Route::post('/product', 'AdminProductController@store')->name('admin.product.store');
    Route::get('/product/{id}/edit', 'AdminProductController@edit')->name('admin.product.edit');
    Route::post('/product/{id}', 'AdminProductController@update')->name('admin.product.update');
    Route::get('/product/{id}/destroy', 'AdminProductController@destroy')->name('admin.product.destroy');

    Route::get('/kjskb', 'AdminKjskbController@kjskb')->name('admin.kjskb.index');
    Route::get('/kjskb', 'AdminKjskbController@index')->name('admin.kjskb.index');
    Route::get('/kjskb/create', 'AdminKjskbController@create')->name('admin.kjskb.create');
    Route::post('/kjskb', 'AdminKjskbController@store')->name('admin.kjskb.store');
    Route::get('/kjskb/{id}/edit', 'AdminKjskbController@edit')->name('admin.kjskb.edit');
    Route::post('/kjskb/{id}', 'AdminKjskbController@update')->name('admin.kjskb.update');
    Route::get('/kjskb/{id}/destroy', 'AdminKjskbController@destroy')->name('admin.kjskb.destroy');

    Route::get('/cesium', 'AdminCesiumController@index')->name('admin.cesium.index');
    Route::get('/cesium/create', 'AdminCesiumController@create')->name('admin.cesium.create');
    Route::get('/cesium/token', 'AdminCesiumController@token')->name('admin.cesium.token');
    Route::post('/cesium/token', 'AdminCesiumController@updateToken')->name('admin.cesium.updateToken');
    Route::post('/cesium', 'AdminCesiumController@store')->name('admin.cesium.store');
    Route::get('/cesium/{id}/edit', 'AdminCesiumController@edit')->name('admin.cesium.edit');
    Route::post('/cesium/{id}', 'AdminCesiumController@update')->name('admin.cesium.update');
    Route::get('/cesium/{id}/destroy', 'AdminCesiumController@destroy')->name('admin.cesium.destroy');

    Route::get('/page/{name}', 'AdminPageController@edit')->name('admin.page.edit');
    Route::post('/page/{name}', 'AdminPageController@store')->name('admin.page.update');

    Route::get('/user', 'AdminUserController@index')->name('admin.user.index');
    Route::get('/user/edit', 'AdminUserController@edit')->name('admin.user.edit');
    Route::post('/user', 'AdminUserController@update')->name('admin.user.update');

    Route::get('/logout', function() {
        Auth::logout();
        return redirect()->route('login');
    });
});

Route::prefix('admin')->group(function() {
    Auth::routes();

    // Route::get('/register', function() {
    //     return view('admin.register');
    // })->name('admin.register');

    // Route::post('/register', function(UserRegisterRequest $request) {
    //     $user = new User;
    //
    //     $user->name = $request->name;
    //     $user->username = $request->username;
    //     $user->email = $request->email;
    //     $user->password = bcrypt($request->password);
    //
    //     if($user->save()) {
    //         \Session::flash('success-saved');
    //         return view('admin.submit');
    //     }
    //
    //     \Session::flash('error-saved');
    //     return redirect()->back();
    // })->name('admin.submit');
});



// Route::get('/home', 'HomeController@index')->name('home');
